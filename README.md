# KNOBB - connected stove project #

This repository contains snippets of work that I did with **QULP**- a product development startup in 2015. 

The project **knobb** is a connected stove which makes monitoring and control of a stove easier and user-friendly. The files here contain mechanical design for our stove embedded with a timer and initial versions for arduino files which can control the audible(speaker) and bluetooth control notifications for the smartphone 

![Assem 2.JPG](https://bitbucket.org/repo/Aj68kn/images/1204288036-Assem%202.JPG)