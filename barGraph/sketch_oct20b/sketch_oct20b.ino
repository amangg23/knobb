  /* define all the led points in the system oen by one */

int ledpin1=2;
int ledpin2=3;
int ledpin3=4;
int ledpin4=5;

//end of all the led points

void setup() {
  // initialize serial communication at 9600 bits per second:
  // also initialize all the ledpins for either output or input

  
  Serial.begin(9600);
  pinMode(ledpin1,OUTPUT);
  pinMode(ledpin2,OUTPUT);
  pinMode(ledpin3,OUTPUT);

  // end of all the ledpins and points
}


void loop() {
  
  int sensorValue = analogRead(A0);  // read the input on analog pin 0 for the value of the potentiometer
  
  Serial.println(sensorValue);       // print out the value you read to the serial monitor
  delay(1);  
  // delay in between reads for stability

int resolution= 1023/16 ;           // resolution is equivalent to the value that will light one single led

/* Loop statements that will initiate the loop for glowing all the led */

if (sensorValue < resolution)
{
  digitalWrite(ledpin1,HIGH);
  }
else if (sensorValue < (resolution*2) )

{
  digitalWrite(ledpin1,HIGH);
  digitalWrite(ledpin2,HIGH);
  
  }
else if (sensorValue < (resolution*3))

  {
    
    digitalWrite(ledpin1,HIGH);
    digitalWrite(ledpin2,HIGH);
    digitalWrite(ledpin3,HIGH);
    }
  


else 
{
  digitalWrite(ledpin1,LOW);
  digitalWrite(ledpin2,LOW);
  digitalWrite(ledpin3,LOW);
  
  }
}
  
