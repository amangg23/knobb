/* THIS IS THE TEST MODEL FOR THE FIRST PROTOTYPE OF THE 
 * KNOBBER WITH MELODY TONE PLAYING AND LED DISPLAY MECHANISM
 *  
 */
  
  
  
  /* define all the led points in the system oen by one */


/* Play Melody
 * -----------
 *
 * Program to play a simple melody
 *
 * Tones are created by quickly pulsing a speaker on and off 
 *   using PWM, to create signature frequencies.
 *
 * Each note has a frequency, created by varying the period of 
 *  vibration, measured in microseconds. We'll use pulse-width
 *  modulation (PWM) to create that vibration.

 * We calculate the pulse-width to be half the period; we pulse 
 *  the speaker HIGH for 'pulse-width' microseconds, then LOW 
 *  for 'pulse-width' microseconds.
 *  This pulsing creates a vibration of the desired frequency.
 *
 * (cleft) 2005 D. Cuartielles for K3
 * Refactoring and comments 2006 clay.shirky@nyu.edu
 * See NOTES in comments at end for possible improvements
 */

// TONES  ==========================================
// Start by defining the relationship between 
//        note, period, &  frequency. 

#define  D     3830    // 261 Hz 
#define  Z     3400    // 294 Hz 
#define  e     3038    // 329 Hz 
#define  f     2864    // 349 Hz 
#define  g     2550    // 392 Hz 
#define  a     2272    // 440 Hz 
#define  B     2028    // 493 Hz 
#define  C     1912    // 523 Hz 

// Define a special note, 'R', to represent a rest
#define  R     0


// SETUP ============================================
// Set up speaker on a PWM pin (digital 9, 10 or 11)
int speakerOut = A2;
// Do we want debugging on serial out? 1 for yes, 0 for no
int DEBUG = 1;



int ledpin[]= {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
int soundpin = A2;

//end of all the led points


void setup() {
  // initialize serial communication at 9600 bits per second:
  // also initialize all the ledpins for either output or input

  
  Serial.begin(9600);
  for (int i=0; i<14; i++)
  {
    pinMode(ledpin[i],OUTPUT);
    }

pinMode(soundpin,OUTPUT);    
  
  // end of all the ledpins and points

//beginning of the speaker out setup
pinMode(speakerOut, OUTPUT);
  if (DEBUG) { 
    Serial.begin(9600); // Set serial out if we want debugging
  } 

}


/* here begins the melody tuning */
// MELODY and TIMING  =======================================
//  melody[] is an array of notes, accompanied by beats[], 
//  which sets each note's relative length (higher #, longer note) 
int melody[] = {  C,  B,  g,  C,  B,   e,  R,  C,  D,  g, a, C };
int beats[]  = { 16, 16, 16,  8,  8,  16, 32, 16, 16, 16, 8, 8 }; 
int MAX_COUNT = sizeof(melody) / 2; // Melody length, for looping.

// Set overall tempo
long tempo = 10000;
// Set length of pause between notes
int pause = 1000;
// Loop variable to increase Rest length
int rest_count = 100; //<-BLETCHEROUS HACK; See NOTES

// Initialize core variables
int tone_ = 0;
int beat = 0;
long duration  = 0;

// PLAY TONE  ==============================================
// Pulse the speaker to play a tone for a particular duration
void playTone() {
  long elapsed_time = 0;
  if (tone_ > 0) { // if this isn't a Rest beat, while the tone has 
    //  played less long than 'duration', pulse speaker HIGH and LOW
    while (elapsed_time < duration) {

      digitalWrite(speakerOut,HIGH);
      delayMicroseconds(tone_ / 2);

      // DOWN
      digitalWrite(speakerOut, LOW);
      delayMicroseconds(tone_ / 2);

      // Keep track of how long we pulsed
      elapsed_time += (tone_);
    } 
  }
  else { // Rest beat; loop times delay
    for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
      delayMicroseconds(duration);  
    }                                
  }                                 
}


void loop() {
  
  int sensorValue = analogRead(A0);  // read the input on analog pin 0 for the value of the potentiometer
  int j,k;
  int timeValue;
  timeValue = map (sensorValue,0,1023,0,60);
  sensorValue = map (sensorValue,0,1023,0,16);
  
  Serial.println(sensorValue);       // print out the value you read to the serial monitor
  delay(1);  
  // delay in between reads for stability

/* Loop statements that will initiate the loop for glowing all the led */

/* alternative test for the switch case */
{
if (sensorValue=0){
  for (int n=0;n<16;n++)
  {
    digitalWrite(ledpin[n],LOW);
    }
  
  }

else{  

for (k=0;k<sensorValue;k++)
    {
    digitalWrite(ledpin[k],HIGH);
    }
for (j=sensorValue;j<16;j++)
    {
      digitalWrite(ledpin[j],LOW);
      }
}      
}
/*end of alternative test for the switch case */


/* beginning of the timer  */

if (timeValue == 3){
    // Set up a counter to pull from melody[] and beats[]
  for (int m=0; m<MAX_COUNT; m++) {
    tone_ = melody[m];
    beat = beats[m];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(pause);

    if (DEBUG) { // If debugging, report loop, tone, beat, and duration
      Serial.print(m);
      Serial.print(":");
      Serial.print(beat);
      Serial.print(" ");    
      Serial.print(tone_);
      Serial.print(" ");
      Serial.println(duration);
    }
    
    
  } }
else {
  
digitalWrite(speakerOut,LOW);  
  }    
}


  
